import {combineReducers } from 'redux';

import products from './products';
import categories from './categories';
import filters from './filters';

export default combineReducers({
    products,
    categories,
    filters
})