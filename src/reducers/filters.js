
const initialState = {
    filterText: '',
    inStockOnly: false
};

export default function filters (state = initialState, action) {
    if (action.type === 'FIND_PRODUCT') {
        return {
            ...state,
            filterText: action.payload
        };
    }
    if (action.type === 'IN_STOCK_CHANGE') {
        return {
            ...state,
            inStockOnly: action.payload
        };
    }
    return state;
}