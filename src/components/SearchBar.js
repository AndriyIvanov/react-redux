import React from 'react';
import { connect } from 'react-redux'

class SearchBar extends React.Component {
    constructor(props) {
      super(props);
      this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
      this.handleInStockChange = this.handleInStockChange.bind(this);
    }
    
    handleFilterTextChange(e) {
        this.props.onFindProduct(e.target.value);
    }
    
    handleInStockChange(e) {
        this.props.onInStockChange(e.target.checked);
    }
    
    render() {
      return (
        <form>
          <input
            type="text"
            placeholder="Search..."
            value={this.props.filterText}
            onChange={this.handleFilterTextChange}
          />
          <p>
            <input
              type="checkbox"
              checked={this.props.inStockOnly}
              onChange={this.handleInStockChange}
            />
            {' '}
            Only show products in stock
          </p>
        </form>
      );
    }
  }

  export default connect(
    state => ({
        testStore: state
      }),
    dispatch => ({
        onFindProduct: (name) => {
          dispatch ({type: 'FIND_PRODUCT', payload: name});
        },
        onInStockChange: (condition) => {
          dispatch ({type: 'IN_STOCK_CHANGE', payload: condition})
        }
    })
  )(SearchBar);