import React, { Component } from 'react';
import { connect } from 'react-redux'

import ProductTable from './components/ProductTable'
import SearchBar from './components/SearchBar'

class App extends Component {
  render() {
    return (
      <div>
        <SearchBar filterText = {this.props.testStore.filters.filterText}
                   inStockOnly = {this.props.testStore.filters.inStockOnly}>
        </SearchBar>
        <ProductTable products = {this.props.testStore.products}
                      filterText = {this.props.testStore.filters.filterText}
                      inStockOnly = {this.props.testStore.filters.inStockOnly}>
        </ProductTable>
      </div>
    );
  }
}

export default connect(
  state => ({
      testStore: state
    })
)(App);
